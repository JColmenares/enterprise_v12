# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * account_deferred_revenue
# 
# Translators:
# Martin Trigaux, 2018
# Alina Lisnenko <alinasemeniuk1@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-18 10:05+0000\n"
"PO-Revision-Date: 2018-09-18 10:05+0000\n"
"Last-Translator: Alina Lisnenko <alinasemeniuk1@gmail.com>, 2018\n"
"Language-Team: Ukrainian (https://www.transifex.com/odoo/teams/41243/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: uk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_asset_sale_tree
msgid "Assets"
msgstr "Активи"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Assets in running state"
msgstr "Активи в активному стані"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Category"
msgstr "Категорія"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Category of asset"
msgstr "Категорія активу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Company"
msgstr "Компанія"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Compute Revenue"
msgstr "Вирахувати дохід"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Confirm"
msgstr "Підтвердити"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Cumulative Revenue"
msgstr "Сукупний дохід"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Current Revenue"
msgstr "Поточний дохід"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_asset_sale_tree
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Customer"
msgstr "Клієнт"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Date of Revenue Sales"
msgstr "Дата доходів з продажів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Date of asset"
msgstr "Дата активу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_invoice_revenue_recognition_category
msgid "Deferred Revenue"
msgstr "Доходи майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.action_account_revenue_report_graph
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.action_account_revenue_report_pivot
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Deferred Revenue Analysis"
msgstr "Аналіз доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_invoice_asset_form
msgid "Deferred Revenue Type"
msgstr "Тип доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model:ir.actions.act_window,name:account_deferred_revenue.action_account_asset_asset_list_normal_sale
#: model:ir.ui.menu,name:account_deferred_revenue.menu_action_account_asset_asset_list_normal_sale
msgid "Deferred Revenue Types"
msgstr "Типи доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Deferred Revenue name"
msgstr "Назва доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model:ir.actions.act_window,name:account_deferred_revenue.action_account_revenue_form
#: model:ir.actions.act_window,name:account_deferred_revenue.action_asset_revenue_report
#: model:ir.ui.menu,name:account_deferred_revenue.menu_action_account_revenue_recognition
#: model:ir.ui.menu,name:account_deferred_revenue.menu_action_asset_revenue_report
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.res_config_settings_view_form
msgid "Deferred Revenues"
msgstr "Доходи майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Draft"
msgstr "Чернетка"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Extended Filters..."
msgstr "Розширені фільтри..."

#. module: account_deferred_revenue
#: model_terms:ir.actions.act_window,help:account_deferred_revenue.action_asset_revenue_report
msgid ""
"From this report, you can have an overview of your deferred revenue. The\n"
"            search bar can also be used to personalize your revenue recognition reporting."
msgstr ""
"З цього звіту ви можете отримати огляд доходів майбутніх періодів.\n"
"            Панель пошуку також може використовуватися для персоналізації звіту про визнання доходу."

#. module: account_deferred_revenue
#: model:ir.ui.menu,name:account_deferred_revenue.menu_recognition_depreciation_confirmation_wizard
msgid "Generate Deferred Revenues Entries"
msgstr "Створення записів доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Gross value of asset"
msgstr "Валова вартість активу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Group By..."
msgstr "Групувати за..."

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Invoice"
msgstr "Рахунок"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Items"
msgstr "Елементи"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Modify Revenue"
msgstr "Змінити дохід"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Next Period Revenue"
msgstr "Наступний період доходу"

#. module: account_deferred_revenue
#: model_terms:ir.actions.act_window,help:account_deferred_revenue.action_asset_revenue_report
msgid "No deferred revenue"
msgstr "Немає доходів майбутніх періодів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Number of Revenues"
msgstr "Кількість доходів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Partner"
msgstr "Партнер"

#. module: account_deferred_revenue
#: model:ir.actions.act_window,name:account_deferred_revenue.action_recognition_depreciation_confirmation_wizard
msgid "Post Installment Lines"
msgstr "Опублікувати рядки платежів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Posted"
msgstr "Опубліковано"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Posted depreciation lines"
msgstr "Опубліковані рядки амортизації"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Recognition in draft state"
msgstr "Визнання у чорновому стані"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Residual"
msgstr "Залишок"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue"
msgstr "Дохід"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue Board"
msgstr "Шкала доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Revenue Date"
msgstr "Дата доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue Entry"
msgstr "Запис доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue Information"
msgstr "Інформація доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue Lines"
msgstr "Рядки доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_asset_sale_tree
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Revenue Name"
msgstr "Назва доходу"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Revenue Recognition"
msgstr "Визнання доходів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Running"
msgstr "Діючий"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_asset_revenue_report_search
msgid "Sales Date"
msgstr "Дата продажів"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Sell or Dispose"
msgstr "Продаж або ліквідація"

#. module: account_deferred_revenue
#: model_terms:ir.ui.view,arch_db:account_deferred_revenue.view_account_asset_revenue_form
msgid "Set to Draft"
msgstr "Зробити чернеткою"
